import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import Welcome from '@/views/Welcome.vue';
import store from '@/store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/logout',
    name: 'logout',
    component: {
      beforeRouteEnter: (to, from, next) => {
        to; from;
        store.dispatch('logout');
        next('/welcome');
      },
    },
  },
  {
    path: '/welcome',
    name: 'welcome',
    component: Welcome,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  let ignoreValidate = [ '/welcome', '/logout' ];
  // Check unverified accounts
  if (store.state.logged && store.state.user &&
      !store.state.user.verified && ignoreValidate.indexOf(to.path) == -1) {
    // Logged into unverified account at any route
    next('/welcome#validate');
  }
  else if (store.state.logged && store.state.user &&
    !store.state.user.first_setup && ignoreValidate.indexOf(to.path) == -1) {
    // Logged into unverified account at any route
    next('/welcome#setup');
  }
  // Guarded route
  else if (to.matched.some((record) => record.meta.requiresAuth)) {
    // Logged
    if (store.state.logged) {
      next();
    }
    // Not logged in protected route
    else {
      next('/welcome');
    }
  }
  else
    next();
});

export default router;
