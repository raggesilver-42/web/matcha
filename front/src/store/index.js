import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

async function signFunction(context, url, data) {
  let res = await axios.post(url, data);
  let { token, user } = res.data;

  localStorage.setItem('token', token);
  localStorage.setItem('user', JSON.stringify(user));

  context.commit('setToken', token);
  context.commit('setUser', user);

  return res;
}

let saveTO = null;

function saveMatchesCache(state) {
  if (saveTO) {
    clearTimeout(saveTO);
    saveTO = null;
  }

  saveTO = setTimeout(() => {
    let copy = JSON.parse(JSON.stringify(state.matches));

    // Don't cache user status
    for (const key in copy) {
      copy[key].online = false;
    }

    localStorage.setItem('matches', JSON.stringify(copy));
    // console.log('[Store] Matches cached');
    saveTO = null;
  }, 3000);
}

export default new Vuex.Store({
  state: {
    logged: !!localStorage.getItem('token'),
    token: localStorage.getItem('token'),
    user: JSON.parse(localStorage.getItem('user')) || null,
    // New cache for both matches and messages. E.g.:
    // {
    //   '12e9asdnnjk2bad0': {
    //     _id: '12e9asdnnjk2bad0',
    //     name: { ... },
    //     media: [],
    //     match: {}, // new location for match info
    //     messages: [], // new location for messages
    //     online: false, // new location for online status
    //   },
    //   {},
    //   {},
    //   {},
    // }
    matches: JSON.parse(localStorage.getItem('matches')) || {},
    notifications: JSON.parse(localStorage.getItem('notifications')) || [],

    locationSet: false,
    viewProfile: false,

    msgUser: null,
  },
  mutations: {
    setToken(state, data) {
      state.token = data;
      state.logged = !!data;

      if (data)
        axios.defaults.headers.common['Authorization'] = `Bearer ${data}`;
      else
        delete axios.defaults.headers.common['Authorization'];
    },
    setUser(state, data) {
      state.user = data;
      let str = JSON.stringify(data);
      if (localStorage.getItem('user') !== str)
        localStorage.setItem('user', str);
    },
    setMsgUser(state, data) {
      state.msgUser = data;
    },
    newMessage(state, message) {
      // console.log('New message state');
      let target = (message.from == state.user._id) ? message.to : message.from;
      if (!(target in state.matches))
        throw new Error('Message from an unkown contact');
      state.matches[target].messages.push(message);
      saveMatchesCache(state);
    },
    logout(state) {
      state.token = null;
      state.logged = false;
      state.user = null;
      state.messages = {};
      state.msgUser = null;
    },
  },
  actions: {
    async login(ctx, data) {
      return await signFunction(ctx, '/api/auth/login', data);
    },
    async register(ctx, data) {
      return await signFunction(ctx, '/api/auth/register', data);
    },
    // TODO: check if this action needs to be async
    async logout(context) {
      localStorage.clear();
      context.commit('logout');
      delete axios.defaults.headers.common['Authorization'];
    },
    async getUser(context) {
      let { data } = await axios.get('/api/user/me');

      localStorage.setItem('user', JSON.stringify(data));
      context.commit('setUser', data);
      return data;
    },
    async getConversation(context, _with) {
      axios.get(`/api/message/with/${_with._id}`)
        .then(({ data }) => {
          // console.log('Got messages with ' + _with._id, data);
          Vue.set(context.state.matches[_with._id], 'messages', data);
          saveMatchesCache(context.state);
        })
        .catch((err) => err);
    },
    async getMatches(context) {
      if (!context.state.logged) return false;

      let { data } = await axios.get('/api/user/matches');

      let ids = new Array();

      // Extract matches from the result and configure them as specified at ln42
      data.forEach((m) => {
        let match = {
          ...m.users[0],
          match: {
            since: m.createdAt,
            _id: m._id,
          },
          messages: context.state.matches[m.users[0]._id] ?
            context.state.matches[m.users[0]._id].messages : [],
        };

        ids.push(m.users[0]._id);

        Vue.set(context.state.matches, m.users[0]._id, match);
        context.dispatch('getConversation', m.users[0]);
      });

      for (const key in context.state.matches) {
        if (ids.indexOf(key) === -1)
          Vue.delete(context.state.matches, key);
      }

      saveMatchesCache(context.state);
      return data;
    },
  },
  modules: {
  },
  getters: {
    currentMessages(state) {
      return (state.msgUser) ? state.matches[state.msgUser._id].messages : [];
    },
  },
});
