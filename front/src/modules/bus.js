import Vue from 'vue';
import axios from 'axios';
import Io from 'socket.io-client';

export default (router, store) => {
  let bus = new Vue({ router });

  bus.$io = null;

  bus.$on('init-socket', () => {
    // if (bus.$io)
    //   return console.log('Socket already setup');

    let query = { token: store.state.token, max_reconnection_attempts: 0 };
    bus.$io = Io(axios.defaults.baseURL, { query });

    bus.$io.on('location-set', (loc) => {
      Vue.set(store.state.user, 'location', loc);
      Vue.set(store.state, 'locationSet', true);
    });

    bus.$io.on('connect', () => {
      // Get geolocation
      if (!store.state.user.settings.location_enabled || !navigator.geolocation)
        return bus.$io.emit('geo-fail');

      navigator.geolocation.getCurrentPosition(
        (pos) => {
          bus.$io.emit('set-location', {
            lat: pos.coords.latitude,
            lon: pos.coords.longitude
          });
        },
        () => bus.$io.emit('geo-fail')
      );
    });

    bus.$io.on('notification', (data) => {
      Vue.set(store.state, 'notifications', [ ...store.state.notifications, data ]);
    });

    bus.$io.on('new-request', (data) => {
      // console.log('New request', data);
      bus.$emit('new-request', data.request);
    });

    bus.$io.on('new-match', (data) => {
      // console.log('New match', data);
      bus.$emit('new-match', data.match);
      store.dispatch('getMatches');
    });

    bus.$io.on('message', async ({ message }) => {
      store.commit('newMessage', message);
    });

    bus.$io.on('disconnect-duplicate', () => {
      window.alert(
        'You activated matcha on another tab. This one is now deactivated');
    });

    bus.$io.on('user-online', ({ _id }) => {
      bus.$emit('user-online', _id);
    });

    bus.$io.on('user-offline', ({ _id }) => {
      bus.$emit('user-offline', _id);
    });

    bus.$io.on('remove-match', ({ _id }) => {
      if (_id in store.state.matches) {
        if (store.state.msgUser === _id)
          Vue.set(store.state, 'msgUser', null);
        Vue.delete(store.state.matches, _id);
      }
    });
  });

  bus.$on('logout', () => {
    store.dispatch('logout');
    router.push('/welcome');
  });

  // TODO: logout
  bus.$watch('$route', () => {
    if (!bus.$io) return;
    if (!store.state.logged) {
      bus.$io.disconnect();
      bus.$io = null;
    }
  });

  return bus;
};
