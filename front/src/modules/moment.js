
const moment = {
  /**
   * Get a string in format <time>[y,M,w,d,h,m,s]
   * @param {Date} date
   */
  timeSince(date) {
    if (!(date instanceof Date))
      date = new Date(date);

    let seconds = Math.floor(
      (new Date().getTime() / 1000) - (date.getTime() / 1000)
    );
    let interval;

    interval = Math.floor(seconds / (60 * 60 * 24 * 7 * 4 * 12));
    if (interval >= 1) return interval + ' years';

    interval = Math.floor(seconds / (60 * 60 * 24 * 7 * 4));
    if (interval >= 1) return interval + ' months';

    interval = Math.floor(seconds / (60 * 60 * 24 * 7));
    if (interval >= 1) return interval + ' weeks';

    interval = Math.floor(seconds / (60 * 60 * 24));
    if (interval >= 1) return interval + ' days';

    interval = Math.floor(seconds / (60 * 60));
    if (interval >= 1) return interval + ' hours';

    interval = Math.floor(seconds / 60);
    if (interval >= 1) return interval + ' minutes';

    return Math.floor(seconds) + ' seconds';
  }
};

export default moment;
export const timeSince = moment.timeSince;
