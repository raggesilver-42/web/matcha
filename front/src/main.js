import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
import AsyncButton from '@/components/AsyncButton.vue';
import Error from '@/components/Error.vue';
import Bus from '@/modules/bus';

Vue.config.productionTip = false;
Vue.config.devtools = true;

let url = new URL(window.location.href);

if (url.port == '8080')
  url.port = '3001';

axios.defaults.baseURL = url.origin;

axios.interceptors.response.use(null, (err) => {
  if (!err.response) {
    let e = { ...err };
    e.response = { data: { error: 'Network Error' }, status: 503, };
    return Promise.reject(e);
  }
  else
    return Promise.reject(err);
});

let token = localStorage.getItem('token');
if (token)
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;

/**
 * Global vue instance used to emit and listen to events. Must be created after
 * axios was setup
 */
Vue.prototype.$bus = Bus(router, store);

Vue.component('v-icon', Icon);
Vue.component('AsyncButton', AsyncButton);
Vue.component('Error', Error);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
