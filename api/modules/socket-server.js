const Io       = require('socket.io');
// eslint-disable-next-line no-unused-vars
const Message  = require('../models/message');
// eslint-disable-next-line no-unused-vars
const Request  = require('../models/request');
// eslint-disable-next-line no-unused-vars
const Match    = require('../models/match');
const User     = require('../models/user');
const jwt      = require('jsonwebtoken');
// eslint-disable-next-line no-unused-vars
const mongoose = require('mongoose');
const axios    = require('axios');
const Mailer   = require('./mailer');

class _Ipdata {
  constructor(key) {
    this.key = key;
    this.baseurl = `https://api.ipdata.co`;
  }

  async getLocation(ip) {
    let _ip = ip;
    if (_ip === '::1')
      _ip = '';
    if (_ip.indexOf('::') !== -1) {
      let parts = _ip.split(':');
      _ip = parts[parts.length - 1];
    }

    try {
      let { data } = await axios.get(
        `${this.baseurl}/${_ip}?api-key=${this.key}`
      );
      return { lat: data.latitude, lon: data.longitude };
    }
    catch(e) {
      if (e.response &&
          e.response.data.message.indexOf('is a private IP address') !== -1)
        return await this.getLocation('');
      else throw e;
    }
  }
}

const Ipdata = new _Ipdata(process.env.IPDATA_KEY);

async function _guard(token) {
  return new Promise((resolve) => {
    jwt.verify(token, process.env.JWT_KEY, async (err, data) => {
      if (err)
        return resolve(false);

      let user = null;

      try {
        user = await User.findById(data._id)
          .select('-password')
          .populate('matches');
      }
      catch(e) {
        e;
        return resolve(false);
      }

      return resolve((user) || false);
    });
  });
}

async function updateUser(soc) {
  try {
    let user = await User.findById(soc._user._id)
      .select('-password')
      .populate('matches');

    // eslint-disable-next-line require-atomic-updates
    soc._user = user;
  }
  catch(e) {
    e;
  }
}

module.exports = class SocketServer {
  constructor(http) {
    this.io = new Io(http);

    this.io.use(this.authorizeConnection.bind(this));
    this.io.on('connection', this.handleConnection.bind(this));

    this.sockets = {};
  }

  /**
   * @param {mongoose.Schema.Types.ObjectId} user_id
   */
  getSocket(user_id) {
    return this.sockets[user_id.toString()] || null;
  }

  /**
   * @param {Message} message
   */
  async sendMessage(message) {
    try {
      let to_soc = this.getSocket(message.to);
      let from_soc = this.getSocket(message.from);

      /**
       * New approach sends the message through sockets to both the sender and
       * the receiver
       */
      if (to_soc)
        to_soc.emit('message', {
          message
        });
      if (from_soc)
        from_soc.emit('message', {
          message
        });
    }
    catch(e) {
      e;
    }
  }

  /**
   * @param {Request} request
   */
  async sendNewRequest(request) {
    try {
      let to_soc = this.getSocket(request.to);
      // Target user is not online
      if (!to_soc) return;

      to_soc.emit('new-request', {
        request,
      });
    }
    catch(e) {
      e;
    }
  }

  /**
   * Send a notification to the user who first liked the other (as the latter
   * just accepted the match request)
   * @param {Match} match
   */
  async sendNewMatch(match) {
    try {
      // to_soc is the socket of the first user (the one who first liked)
      let from_soc = this.getSocket(match.users[1]);
      // Calling updateUser adds the new users to the sockets that receive
      // online/offline notifications
      if (from_soc) await updateUser(from_soc);

      let to_soc = this.getSocket(match.users[0]);
      if (!to_soc) return;

      await updateUser(to_soc);

      // to is the user who just accepted the match
      let to = (await User.findById(match.users[1])).getPublicData();
      let m = match.toObject();

      m.users[1] = to;

      to_soc.emit('new-match', {
        match: m,
      });
    }
    catch(e) {
      e;
    }
  }

  async removeMatch(user, removed) {
    let us = this.getSocket(user._id);
    if (us) {
      await updateUser(us);
      us.emit('remove-match', { _id: removed._id });
    }

    let rf = this.getSocket(removed._id);
    if (rf) {
      await updateUser(rf);
      rf.emit('remove-match', { _id: user._id });
    }
  }

  sendNotification(user, notification) {
    let soc = this.getSocket(user._id);
    let not = {
      _id: new mongoose.Types.ObjectId(),
      ...notification,
      read: false,
    };

    if (soc) {
      soc.emit('notification', not);
    } else if (user.settings.email_notify) {
      Mailer.sendFromTemplate('notification', {
        from: process.env.MAIL_USER,
        to: user.email,
        subject: 'Matcha | Notification',
      }, { user, text: not.text })
        .catch((err) => err);
    }
  }

  async authorizeConnection(soc, next) {
    if (soc.handshake.query && soc.handshake.query.token) {
      let user = await _guard(soc.handshake.query.token);

      if (user) {
        // eslint-disable-next-line require-atomic-updates
        soc._user = user;
        return next();
      }
    }
    // console.log('Refused connection due to auth', soc.id, new Date());
    // console.log(soc.handshake.query);
    return next(new Error('Not authenticated'));
  }

  handleConnection(soc) {
    // console.log(`[Socket server] ${soc._user.fullname} connected.`);

    // Make connections
    soc.once('disconnect', () => {
      // console.log(`[Socket server] ${soc._user.fullname} disconnected.`);
      soc._user.last_active = Date.now();
      soc._user.save()
        .catch ((e) => e);
      delete this.sockets[soc._user._id.toString()];
      // Notify offline
      soc._user.matches.forEach((m) => {
        m.users.filter((id) => id !== soc._user._id.toString()).forEach((id) => {
          let s = this.getSocket(id);
          if (s) s.emit('user-offline', { _id: soc._user._id });
        });
      });
    });

    if (soc._user._id.toString() in this.sockets) {
      let s = this.sockets[soc._user._id.toString()];
      s.emit('disconnect-duplicate');
      s.disconnect();
      delete this.sockets[soc._user._id.toString()];
    }
    // Add socket to "database"
    this.sockets[soc._user._id.toString()] = soc;

    let locationSet = false;

    // Location connections
    soc.once('set-location', async (loc) => {
      if (locationSet) return;
      else locationSet = true;
      try {
        soc._user.location = loc;
        await soc._user.save();
        soc.emit('location-set', soc._user.location);
      }
      catch(e) { e; }
    });

    soc.on('geo-fail', async () => {
      if (locationSet) return;
      else locationSet = true;
      try {
        // eslint-disable-next-line require-atomic-updates
        soc._user.location = await Ipdata.getLocation(
          soc.request.connection.remoteAddress);
        await soc._user.save();
        soc.emit('location-set', soc._user.location);
      }
      catch(e) { e; }
    });

    // Notify online
    soc._user.matches.forEach((m) => {
      m.users.filter((id) => id !== soc._user._id.toString()).forEach((id) => {
        let s = this.getSocket(id);
        if (s) s.emit('user-online', { _id: soc._user._id });
      });
    });
  }
};
