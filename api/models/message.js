const mongoose  = require('mongoose');

const schema = new mongoose.Schema({
  from:  { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
  to:    { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
  read:  { type: Boolean, default: false },
  text:  { type: String },
  media: [
    { type: String }
  ],
}, {
  timestamps: true,
});

module.exports = mongoose.model('message', schema);
