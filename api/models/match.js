const mongoose  = require('mongoose');

const schema = new mongoose.Schema({
  // This is an array so that it can be queryed with $in
  users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
}, {
  timestamps: true,
});

module.exports = mongoose.model('match', schema);
