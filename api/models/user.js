const mongoose  = require('mongoose');
const jwt       = require('jsonwebtoken');
const bcrypt    = require('bcrypt');
const crypto    = require('crypto');
const Mailer    = require('../modules/mailer');

const schema = new mongoose.Schema({
  name: {
    first:  { type: String },
    last:   { type: String },
  },
  username: { type: String },
  password: { type: String },
  email:    { type: String },
  // Needs setup
  age:      { type: Number },
  media: [
    /**
     * For matcha the user must have from 1 to 5 pictures, by design media[0] is
     * the profile picture. For compatibility there is a user.picture virtual
     */
    { type: String }
  ],
  first_setup: { type: Boolean, default: false },
  sex: {
    type: String,
    enum: [ 'male', 'female', 'other' ],
  },
  sexualPreference: {
    type: String,
    enum: [ 'male', 'female', 'both', 'other' ],
  },
  bio: {
    type: String,
    maxlength: 120,
    default: null
  },
  interests: [
    /**
     * User's hobbies. By my requirement each user must have at least 3.
     */
    { type: mongoose.Schema.Types.ObjectId, ref: 'tag' }
  ],
  // Computed
  fame_rating: {
    /**
     * Fame rating is a calculation of how popular an user is. The calculation
     * is the median of times the profile was seen and times the profile was
     * liked. Dislikes also have a negative effect on fame rating.
     */
    type: Number,
    default: 1,
  },
  likes:    [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
  dislikes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
  matches:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'match' }],
  location: {
    lat: { type: Number },
    lon: { type: Number },
  },
  // Settings
  verified: { type: Boolean, default: false },
  settings: {
    email_notify:     { type: Boolean, default: true },
    location_enabled: { type: Boolean, default: false },
    blocked_users:    [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
  },
  verification: {
    code: {
      tok: { type: String },
      exp: { type: Date }
    },
    reset: {
      tok: { type: String },
      exp: { type: Date }
    }
  },
  last_active: { type: Date },
});

schema.virtual('fullname').get(function () {
  return `${this.name.first} ${this.name.last}`;
});

schema.virtual('picture').get(function () {
  return (this.media && this.media.length > 0) ? this.media[0] : null;
});

schema.statics.validatePassword = (pass) => {
  return /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[~!@#$%^&*()_+-]).{8,}$/.test(pass);
};

schema.statics.hashPassword = async (password) => {
  return await bcrypt.hash(password, 10);
};

schema.methods.comparePassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

schema.methods.getToken = function () {
  return jwt.sign({
    _id: this._id
  }, process.env.JWT_KEY);
};

schema.statics.generateVerificationCode = () => {
  let code = {
    tok: crypto.randomBytes(20).toString('hex'),
    exp: new Date()
  };

  code.exp.setHours(code.exp.getHours() + 1);

  return (code);
};

schema.statics.generateResetCode = () => {
  let code = {
    tok: crypto.randomBytes(20).toString('hex'),
    exp: new Date()
  };

  code.exp.setHours(code.exp.getHours() + 1);

  return (code);
};

/**
 * Generate a new reset code and send it to the user's email.
 * Throws Mailer/Mongoose errors.
 */
schema.methods.sendResetCode = async function () {
  this.verification.reset = this.constructor.generateResetCode();
  await this.save();

  return await Mailer.sendFromTemplate('passwordreset', {
        from: process.env.MAIL_USER,
          to: this.email,
     subject: 'Password reset'
    }, { tok: this.verification.reset.tok, user: this });
};

/**
 * Generate a new code and send it to the user's email.
 * Throws Mailer/Mongoose errors.
 */
schema.methods.sendVerificationCode = async function () {
  this.verification.code = this.constructor.generateVerificationCode();
  await this.save();

  return await Mailer.sendFromTemplate('validate', {
        from: process.env.MAIL_USER,
          to: this.email,
     subject: 'Account validation'
    }, { code: this.verification.code.tok, user: this });
};

schema.methods.getPersonalData = function () {
  let data = this.toObject();

  delete data.password;
  delete data.verification;

  return (data);
};

function toRad(val) {
  return (val * Math.PI / 180);
}

/**
 * Calculate distance in Kilometers between two user locations
 * @param {Object} a
 * @param {Object} b
 */
function distance(a, b) {
  let r = 6371; // Km
  let dLat = toRad(b.lat - a.lat);
  let dLon = toRad(b.lon - a.lon);
  let lata = toRad(a.lat);
  let latb = toRad(b.lat);

  let aa = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lata) * Math.cos(latb);
  let cc = 2 * Math.atan2(Math.sqrt(aa), Math.sqrt(1 - aa));
  let dd = r * cc;
  return (dd);
}

schema.methods.createMatch = function (user) {
  let u = user.getPublicData();
  // Get integer distance
  u.distance = Math.round(distance(this.location, user.location));
  // Create commonInterests array
  u.commonInterests = user.interests.filter((i) => {
    return (this.interests.indexOf(i._id.toString()) !== -1);
  });
  return (u);
};

schema.methods.getPublicData = function () {
  let data = this.toObject();

  delete data.password;
  delete data.verification;
  delete data.email;
  delete data.first_setup;
  delete data.likes;
  delete data.dislikes;
  delete data.matches;
  delete data.settings;
  delete data.verified;
  delete data.__v;
  delete data.location;

  return (data);
};

module.exports = mongoose.model('user', schema);
