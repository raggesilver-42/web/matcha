const mongoose = require('mongoose');

/**
 * Tags are user hobbies. They have their own collection because they can be
 * reusable (they are suggested as the user types a new hobby and can also be
 * used to find users with similar hobbies).
 */
const schema = new mongoose.Schema({
  name: { type: String },
});

module.exports = mongoose.model('tag', schema);
