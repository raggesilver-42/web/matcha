const mongoose  = require('mongoose');

const schema = new mongoose.Schema({
  from:  { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
  to:    { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
}, {
  timestamps: true,
});

module.exports = mongoose.model('request', schema);
