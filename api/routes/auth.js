const router        = require('express').Router();
const User          = require('../models/user');
const { reqparams,
        notEmpty  } = require('@raggesilver/reqparams');
const guard         = require('../modules/guard');

async function validateUsername(val) {
  val = val.trim();

  if (val.length < 6) return 'Username too short (min 6 characters)';
  if (val.length > 20) return 'Username too long (max 20 characters)';

  try {
    let user = await User.findOne({ username: val });
    if (!user) return true;
  }
  catch(e) {
    e;
  }
  return 'Username in use';
}

function validatePassword(val) {
  return /^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[~!@#$%^&*()_+-]).{8,}$/.test(val);
}

async function validateEmail(val) {
  if (!/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
      .test(val))
    return 'Invalid email';

  try {
    let user = await User.findOne({ email: val });
    if (!user) return true;
  }
  catch(e) {
    e;
  }
  return 'Email in use';
}

const validateName = (val) => {
  return (notEmpty(val) && /^[a-zA-ZÀ-ÖØ-öø-ÿ'-]+$/.test(val));
};

const registerPostParams = {
  first_name: { validate: validateName },
  last_name:  { validate: validateName },
  username:   { validate: validateUsername },
  email:      { validate: validateEmail },
  password:   {
    validate: validatePassword,
         msg: 'Password must have at least 8 characters, one special character '
              + 'and one number'
  },
};

router.post('/register', reqparams(registerPostParams), async (req, res) => {
  try {
    let user = new User({
      email: req.body.email,
      username: req.body.username,
      password: await User.hashPassword(req.body.password),
      name: {
        first: req.body.first_name,
        last: req.body.last_name,
      }
    });

    await user.save();
    await user.sendVerificationCode();

    return res.status(200).json({
      token: user.getToken(),
       user: user.getPersonalData()
    });
  }
  catch(e) {
    e;
    return res.status(500).json({ error: 'INTERNAL_ERROR' });
  }
});

const loginPostParams = {
  username: { validate: notEmpty },
  password: { validate: notEmpty },
};

router.post('/login', reqparams(loginPostParams), async (req, res) => {
  try {
    let user = await User.findOne({ username: req.body.username })
      .populate('interests');
    if (!user || !(await user.comparePassword(req.body.password)))
      return res.status(401).json({ error: 'Invalid credentials' });

    let payload = {
      token: user.getToken(),
       user: user.getPersonalData()
    };

    return res.status(200).json(payload);
  }
  catch(e) {
    e;
    return res.status(500).json({ error: 'INTERNAL_ERROR' });
  }
});

router.post('/validate/:tok', guard, async (req, res) => {
  try {
    if (req.user.verified) {
      return res.status(200).json({ user: req.user.getPersonalData() });
    }

    let user = await User.findOne({
      'verification.code.tok': req.params.tok,
      _id: req.user._id,
    });

    if (user) {
      if (new Date(user.verification.code.exp) < new Date())
        return res.status(400).json({ error: 'TOKEN_EXPIRED' });

      user.verification.code = null;
      user.verified = true;

      await user.save();

      return res.status(200).json({ user: req.user.getPersonalData() });
    }
    else {
      return res.status(400).json({ error: 'Invalid token' });
    }
  }
  catch(e) {
    e;
    return res.status(500).json({ error: 'INTERNAL_ERROR' });
  }
});

router.post('/revalidate', guard, async (req, res) => {
  try {
    let response = 'ALREADY_VERIFIED';

    if (!req.user.verified) {
      await req.user.sendVerificationCode();
      response = 'Verification email re-sent';
    }

    return res.status(200).json({ msg: response });
  }
  catch(e) {
    e;
    return res.status(500).json({ error: 'INTERNAL_ERROR' });
  }
});

const rreqPostParams = {
  email: {}, // No validation, just needs to be present
};

router.post('/reset_request', reqparams(rreqPostParams), async (req, res) => {
  try {
    let user = await User.findOne({ email: req.body.email });
    // This approach does not tell the client whether the email was sent or not
    // nor does it say if the user exists. This version also allows unverified
    // users to receive password reset codes
    if (user) {
      await user.sendResetCode();
    }
    return res.status(200).json({});
  }
  catch(e) {
    e;
    return res.status(500).json({ error: 'INTERNAL_ERROR' });
  }
});

const rpasPostParams = {
  tok: {}, // No validation, just needs to be present
  password: {
    validate: validatePassword,
         msg: 'Password must have at least 8 characters, one special character '
              + 'and one number'
  },
};

router.post('/reset_password', reqparams(rpasPostParams), async (req, res) => {
  try {
    let user = await User.findOne({ 'verification.reset.tok': req.body.tok });
    if (user) {
      // Verify that the token is still valid
      if (new Date(user.verification.reset.exp) < new Date())
        return res.status(400).json({ error: 'Token expired' });
      user.verification.reset = null;
      user.password = await User.hashPassword(req.body.password);
      await user.save();
      return res.status(200).json({
        token: user.getToken(),
        user: user.getPersonalData(),
      });
    }
    else {
      return res.status(400).json({ error: 'Invalid token' });
    }
  }
  catch(e) {
    e;
    return res.status(500).json({ error: 'INTERNAL_ERROR' });
  }
});

module.exports = router;
