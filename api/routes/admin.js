const router  = require('express').Router();
const Match   = require('../models/match');
const Message = require('../models/message');
const User    = require('../models/user');
const Request = require('../models/request');

router.post('/reset_matches', async (req, res, next) => {
  try {
    let users = await User.find();

    await Promise.all([
      ...users.map((user) => {
        user.likes = [];
        user.dislikes = [];
        user.matches = [];
        user.settings.blocked_users = [];
        return user.save();
      }),
      Message.deleteMany({}),
      Match.deleteMany({}),
      Request.deleteMany({}),
    ]);

    return res.status(200).json({});
  }
  catch(e) { return next(e); }
});

module.exports = router;
