const router  = require('express').Router();
const User    = require('../models/user');
const Request = require('../models/request');
const Match   = require('../models/match');
const Tag     = require('../models/tag');
const guard   = require('../modules/guard');
const Imgur   = require('../modules/imgur');
const Mailer  = require('../modules/mailer');

const { reqparams, notEmpty } = require('@raggesilver/reqparams');

router.get('/me', guard, async (req, res) => {
  return res.status(200).json(req.user.getPersonalData());
});

let setupMid = [
  guard,
  reqparams({
    sex: {
      type: String,
      validate: (val) => [ 'male', 'female', 'other' ].indexOf(val) != -1,
    },
    sexualPreference: {
      type: String,
      validate: (val) => {
        return [ 'male', 'female', 'both', 'other' ].indexOf(val) != -1;
      },
    },
    bio: {
      type: String,
      validate: (val) => {
        return (typeof val == 'string' && val.length > 0 && val.length < 120);
      },
      optional: true,
    },
    interests: {
      type: Array,
      validate: (val) => {
        for (const v of val) {
          if (typeof v !== 'string') return false;
          if (v.trim().length == 0) return 'Empty interest';
          if (v.trim().length > 30)
            return 'Interest too long (max 30 characters)';
        }
        if (val.length < 3)
          return 'Minimum of 3 interests required';
        return true;
      },
    },
    media: {
      type: Array,
      validate: notEmpty,
    },
  }),
];

router.post('/setup', setupMid, async (req, res, next) => {
  const fields = [ 'bio', 'sex', 'sexualPreference', 'media' ];
  const user   = req.user;

  if (user.first_setup) {
    return res.status(409).json({
      error: 'Account was already setup',
      user: user.getPersonalData(),
    });
  }

  try {
    for (const key in req.body) {
      if (fields.indexOf(key) == -1) continue ;
      user.set(key, req.body[key]);
    }

    if (!user.interests)
      user.interests = new Array();

    // Handle interest tags
    for (const int of req.body.interests) {
      let tag = null;
      if (!(tag = await Tag.findOne({ name: int.toLowerCase() }))) {
        // Create the tag if it doesn't exist yet
        tag = new Tag({ name: int.toLowerCase() });
        await tag.save();
      }
      user.interests.push(tag._id);
    }

    user.first_setup = true;

    await user.save();
    return res.status(200).json(user.getPersonalData());
  }
  catch(e) { next(e); }
});

let updateMid = [
  guard,
  reqparams({
    'name.first': {
      validate: notEmpty,
      optional: true,
    },
    'name.last': {
      validate: notEmpty,
      optional: true,
    },
    'settings.email_notify': {
      type: Boolean,
      optional: true,
    },
    'settings.location_enabled': {
      type: Boolean,
      optional: true,
    },
    username: {
      validate: async (val) => {
        if (val.length < 6) return 'Username too short (min 6 characters)';
        if (val.length > 20) return 'Username too long (max 20 characters)';

        try {
          if (!await User.findOne({ username: val })) return true;
          return 'Username in use';
        }
        catch(_) { return 'An error ocurred changing username'; }
      },
      optional: true,
    },
    email: {
      validate: async (val) => {
        try {
          if (!await User.findOne({ email: val })) return true;
          return 'Email in use';
        }
        catch(_) { return 'An error ocurred changing email'; }
      },
      optional: true,
    },
    newPassword: {
      validate: User.validatePassword,
      msg: 'Password must have at least 8 characters, one special character '
         + 'and one number.',
      optional: true,
    },
    sex: {
      type: String,
      validate: (val) => [ 'male', 'female', 'other' ].indexOf(val) != -1,
      optional: true,
    },
    sexualPreference: {
      type: String,
      validate: (val) => {
        return [ 'male', 'female', 'both', 'other' ].indexOf(val) != -1;
      },
      optional: true,
    },
    bio: {
      type: String,
      validate: (val) => {
        return (typeof val == 'string' && val.length > 0 && val.length < 120);
      },
      optional: true,
    },
    media: {
      type: Array,
      validate: notEmpty,
      optional: true,
    },
  }),
];

router.post('/update', updateMid, async (req, res, next) => {
  const fields = [ 'username', 'name.first', 'name.last',
    'settings.email_notify', 'settings.location_enabled', 'sex',
    'sexualPreference', 'bio', 'media' ];
  const user = req.user;

  try {
    for (const key in req.body) {
      if (fields.indexOf(key) == -1) continue ;
      user.set(key, req.body[key]);
    }

    // Change password
    if ('newPassword' in req.body) {
      if (!req.body.oldPassword ||
          !(await user.comparePassword(req.body.oldPassword)))
        return res.status(400).json({ error: 'Invalid password' });
      user.password = await User.hashPassword(req.body.newPassword);
    }

    // Send the confirmation mail, calls user.save()
    if ('email' in req.body) {
      user.verified = false;
      await user.sendVerificationCode();
    }
    // Save manually
    else {
      await user.save();
    }

    return res.status(200).json(user.getPersonalData());
  }
  catch(e) { return next(e); }
});

let addTagMid = [
  guard,
  reqparams({
    name: {
      type: String,
      validate: (val) => /^.{1,30}$/.test(val),
      msg: 'Interests must have at least 1 and at most 30 characters',
    },
  }),
];

router.post('/add_tag', addTagMid, async (req, res, next) => {
  const user = req.user;
  try {
    // Handle interest tags
    let tag = null;
    if (!(tag = await Tag.findOne({ name: req.body.name.toLowerCase() }))) {
      // Create the tag if it doesn't exist yet
      tag = new Tag({ name: req.body.name.toLowerCase() });
      await tag.save();
    }
    user.interests.push(tag._id);

    return res.status(200).json({});
  }
  catch(e) { return next(e); }
});

router.get('/complete_tag/:tag', guard, async (req, res, next) => {
  try {
    let tags = await Tag.find({ name: { $regex: req.params.tag } });
    return res.status(200).json(tags);
  }
  catch(e) { return next(e); }
});

router.get('/match_feed', guard, async (req, res, next) => {
  try {
    // Get user again because req.user has .populate in some fields
    let user = await User.findById(req.user._id);

    let sort = null;

    if ('sort' in req.query) {
      sort = req.query.sort;
    }

    let filters = {};

    if ('min_age' in req.query && 'max_age' in req.query) {
      filters['age'] = {
        $gte: Number(req.query.min_age),
        $lte: Number(req.query.max_age),
      };
    }

    if ('min_fame' in req.query && 'max_fame' in req.query) {
      filters['fame_rating'] = {
        $gte: Number(req.query.min_fame),
        $lte: Number(req.query.max_fame),
      };
    }

    let matches = await User.find({
      $or: [
        // Straight match
        { sex: user.sexualPreference, sexualPreference: user.sex },
        {
          // For this option if the user is bi, query male and female,
          // otherwise just the user's preference
          sex: (user.sexualPreference == 'both') ?
            { $in: [ 'male', 'female' ] } : user.sexualPreference,
          sexualPreference: { $in: [ user.sex, 'both', ] },
        },
      ],
      // Only match users that have a last known location
      'location.lat': { $exists: true },
      'location.lon': { $exists: true },
      verified: true,
      first_setup: true,
      // Don't match if they liked the user (the user can match the like
      // separately)
      likes: { $ne: user._id },
      // Don't match if they disliked the user
      dislikes: { $ne: user._id },
      // Don't match if the user was blocked by them
      'settings.blocked_users': { $ne: user._id },
      // Don't match if the user liked/disliked/blocked them
      _id: {
        $nin: [
          ...user.likes,
          ...user.dislikes,
          ...user.settings.blocked_users,
          // Don't match ourselves
          user._id,
        ],
      },
      ...filters,
      ...((req.query.filters && JSON.parse(req.query.filters)) || {}),
    }).populate('interests');

    matches = matches.map((m) => user.createMatch(m));

    if ('km_radius' in req.query && !isNaN(req.query.km_radius)) {
      matches = matches.filter((m) => {
        return m.distance <= Number(req.query.km_radius);
      });
    }

    // Sorting by defaults values distance > commonInterests > fame_rating
    matches.sort((a, b) => {
      if (sort == 'distance') {
        // ASC
        if (a.distance < b.distance) return -1;
        if (a.distance > b.distance) return 1;
      } else if (sort == 'commonInterests') {
        // DESC
        if (a.commonInterests.length > b.commonInterests.length) return -1;
        if (a.commonInterests.length < b.commonInterests.length) return 1;
      } else if (sort === 'fame') {
        // DESC
        if (a.fame_rating > b.fame_rating) return -1;
        if (a.fame_rating < b.fame_rating) return 1;
      } else {
        // ASC
        if (a.distance < b.distance) return -1;
        if (a.distance > b.distance) return 1;
        // DESC
        if (a.commonInterests.length > b.commonInterests.length) return -1;
        if (a.commonInterests.length < b.commonInterests.length) return 1;
        // DESC
        if (a.fame_rating > b.fame_rating) return -1;
        if (a.fame_rating < b.fame_rating) return 1;
      }
      return (0);
    });

    if (matches[0]) {
      // NOTE: emails are removed on User.createMatch(), that's why we need this
      User.findById(matches[0]._id)
        .then((target) => {
          req.$ss.sendNotification(target, {
            text: `Your profile was seen by ${req.user.name.first}`,
          });
        })
        .catch(() => {});
    }

    // TODO: I believe we're required to notify a user once their profile is
    // seen. This should be done here
    return res.status(200).json(matches[0] || null);
  }
  catch(e) { return next(e); }
});

router.get('/matches', guard, async (req, res, next) => {
  try {
    let matches = await Match.find({
      users: req.user._id
    });

    // Remove self from result
    matches.forEach((m) => {
      m.users = m.users.filter((u) => u.toString() !== req.user._id.toString());
    });

    matches = await Match.populate(matches, {
      path: 'users',
      select: '-password',
      populate: {
        path: 'interests'
      },
    });

    matches = matches.map((m) => {
      let u0 = m.users[0];
      let res = m.toObject();
      res.users[0] = req.user.createMatch(u0);
      res.users[0].online = !!req.$ss.getSocket(u0._id);
      return (res);
    });

    return res.status(200).json(matches);
  }
  catch(e) { return next(e); }
});

router.get('/requests', guard, async (req, res, next) => {
  try {
    let requests = await Request.find({ to: req.user._id }).populate({
      path: 'from',
      select: '-password',
      populate: {
        path: 'interests',
      },
    });

    requests = requests.map((r) => {
      let res = r.toObject();
      res.from = req.user.createMatch(r.from);
      return (res);
    });

    return res.status(200).json(requests);
  }
  catch(e) { return next(e); }
});

router.post('/dislike/:id', guard, async (req, res, next) => {
  try {
    let target = await User.findById(req.params.id);
    if (!target) return res.status(404).json({ error: 'User not found' });

    if (await Match.findOne({ users: { $all: [ req.user._id, target._id ] }}))
      return res.status(400).json({ error: 'Can\'t dislike a match' });

    // let r = await Request.deleteOne({ from: target._id, to: req.user._id });
    req.user.dislikes.push(target._id);
    await req.user.save();

    // Update target fame_rating
    let target_likes = (await User.countDocuments({ likes: target._id }) || 1);
    let target_dislikes = await User.countDocuments({ dislikes: target._id });
    let fame_result =  target_likes / (target_likes + target_dislikes);
    target.fame_rating = fame_result;

    await target.save();
    req.$ss.sendNotification(target, {
      text: `You were disliked by ${req.user.name.first}`,
    });

    return res.status(200).json({});
  }
  catch(e) { return next(e); }
});

let postRequestMid = [
  guard,
  reqparams({
    target: {
      type: String,
      validate: notEmpty,
    },
  }),
];

router.post('/request', postRequestMid, async (req, res, next) => {
  try {
    let [user, target] = await Promise.all([
      User.findById(req.user._id).populate('interests'),
      User.findById(req.body.target),
    ]);

    if (!target)
      return res.status(404).json({ error: 'User not found' });

    if (await Request.findOne({ from: req.user._id, to: req.body.target }))
      return res.status(409).json({ error: 'Duplicate like' });

    if (target.dislikes.indexOf(req.user._id.toString()) !== -1)
      // Sad :(
      return res.status(400).json({ error: 'That person dislikes you' });

    if (target.settings.blocked_users.indexOf(req.user._id.toString) !== -1)
      // Also sad :(
      return res.status(400).json({ error: 'That person really dislikes you' });

    let request = new Request({
      from: req.user._id,
        to: target._id,
    });

    req.user.likes.push(target._id);

    // Update target fame_rating
    let target_likes = (await User.countDocuments({ likes: target._id }) || 1);
    let target_dislikes = await User.countDocuments({ dislikes: target._id });
    let fame_result =  target_likes / (target_likes + target_dislikes);
    target.fame_rating = fame_result;

    await Promise.all([ req.user.save(), request.save(), target.save() ]);

    let r = request.toObject();
    r.from = target.createMatch(user);

    req.$ss.sendNewRequest(r);
    req.$ss.sendNotification(target, {
      text: `You have a new match request from ${r.from.name.first}`
    });

    return res.status(200).json({});
  }
  catch(e) { return next(e); }
});

router.post('/accept_request/:id', guard, async (req, res, next) => {
  try {
    let request = await Request.findOneAndRemove({
      _id: req.params.id,
      to: req.user._id, // It must be the target accepting the request
    });

    if (!request) return res.status(404).json({ error: 'Request not found' });

    let match = new Match({
      users: [
        request.from,
        request.to,
      ],
    });

    await match.save();

    let target = await User.findById(request.from);

    req.user.matches.push(match._id);
    req.user.likes.push(target._id);
    target.matches.push(match._id);

    // Update target fame_rating
    let target_likes = (await User.countDocuments({ likes: target._id }) || 1);
    let target_dislikes = await User.countDocuments({ dislikes: target._id });
    let fame_result =  target_likes / (target_likes + target_dislikes);
    target.fame_rating = fame_result;

    await Promise.all([ req.user.save(), target.save() ]);

    // TODO: delete all `Request`s involving a user once they delete their acc

    req.$ss.sendNewMatch(match);
    req.$ss.sendNotification(target, {
      text: `You just matched with ${req.user.name.first}`,
    });

    return res.status(200).json({ target: target.getPublicData() });
  }
  catch(e) { return next(e); }
});

router.post('/reject_request/:id', guard, async (req, res, next) => {
  try {
    let request = await Request.findOneAndRemove({
      _id: req.params.id,
      to: req.user._id,
    });

    if (!request) return res.status(404).json({ error: 'Request not found' });

    req.user.dislikes.push(request.from);
    await req.user.save();

    let target = await User.findById(request.from);
    // Update target fame_rating
    let target_likes = (await User.countDocuments({ likes: target._id }) || 1);
    let target_dislikes = await User.countDocuments({ dislikes: target._id });
    let fame_result =  target_likes / (target_likes + target_dislikes);
    target.fame_rating = fame_result;

    await target.save();

    req.$ss.sendNotification(target, {
      text: `You were rejected by ${req.user.name.first}`
    });

    return res.status(200).json({});
  }
  catch(e) { return next(e); }
});

const uploadPicturePostMid = [
  guard,
  reqparams({
    picture: {
      type: String,
      validate: notEmpty,
    },
  }),
];

router.post('/upload_picture', uploadPicturePostMid, async (req, res, next) => {
  try {
    let r = await Imgur.upload(req.body.picture);
    if (r.success) {
      return res.status(200).json({ url: r.data.link });
    }
    else {
      return res.status(r.status).json({ error: r.data.error });
    }
  }
  catch(e) { return next(e); }
});

router.post('/block/:id', guard, async (req, res, next) => {
  try {
    let target = await User.findById(req.params.id);
    if (!target) return res.status(404).json({ error: 'User not found' });

    let match = await Match.deleteOne({ users: { $all: [ req.user._id, target._id ] }});
    if (!match)
      return res.status(400).json({ error: 'Match not found' });

    //remove from matches
    target.matches.pull(req.user._id);
    req.user.matches.pull(target._id);

    req.user.settings.blocked_users.push(target._id);

    await target.save();
    await req.user.save();

    req.$ss.removeMatch(req.user, target);
    req.$ss.sendNotification(target, {
      text: `You were blocked by ${req.user.name.first}`,
    });

    return res.status(200).json(req.user.getPersonalData());
  }
  catch(e) { return next(e); }
});

let reportPostMid = [
  guard,
  reqparams({
    message: {
      type: String,
      validate: notEmpty,
    },
  }),
];

router.post('/report/:id', reportPostMid, async (req, res, next) => {
  try {
    let target = await User.findById(req.params.id);
    if (!target) return res.status(404).json({ error: 'User not found' });

    let match = await Match.deleteOne({ users: { $all: [ req.user._id, target._id ] }});
    if (!match)
      return res.status(400).json({ error: 'Match not found' });

    // send email with reported message
    Mailer.sendFromTemplate('report', {
        from: process.env.MAIL_USER,
          to: process.env.MAIL_USER,
     subject: 'User Reported'
    }, { target, message: req.body.message });

    //remove from matches
    target.matches.pull(req.user._id);
    req.user.matches.pull(target._id);

    req.user.settings.blocked_users.push(target._id);

    await target.save();
    await req.user.save();

    req.$ss.removeMatch(req.user, target);
    req.$ss.sendNotification(target, {
      text: `You were reported by ${req.user.name.first}`,
    });

    return res.status(200).json(req.user.getPersonalData());
  }
  catch(e) { return next(e); }
});

router.post('/unmatch/:id', guard, async (req, res, next) => {
  try {
    let target = await User.findById(req.params.id);
    if (!target) return res.status(404).json({ error: 'User not found' });

    let match = await Match.deleteOne({ users: { $all: [ req.user._id, target._id ] }});
    if (!match)
      return res.status(400).json({ error: 'Match not found' });

    //remove from matches
    target.matches.pull(req.user._id);
    req.user.matches.pull(target._id);

    await target.save();
    await req.user.save();

    req.$ss.removeMatch(req.user, target);
    req.$ss.sendNotification(target, {
      text: `${req.user.name.first} unmatched you`,
    });

    return res.status(200).json(req.user.getPersonalData());
  }
  catch(e) { return next(e); }
});

module.exports = router;
