const router  = require('express').Router();
const User    = require('../models/user');
const Message = require('../models/message');
const Match   = require('../models/match');
const guard   = require('../modules/guard');

const { reqparams, notEmpty } = require('@raggesilver/reqparams');

const mids = {
  postTo: [
    guard,
    reqparams({ text: { type: String, validate: notEmpty, }}),
  ],
};

/**
 * Get all messages between logged user and user:id
 */
router.get('/with/:id', guard, async (req, res, next) => {
  try {
    let target = await User.findById(req.params.id);
    if (!target) return res.status(404).json({ error: 'User not found' });

    let messages = await Message.find({
      $or: [
        { from: req.user._id, to: target._id, },
        { from: target._id, to: req.user._id, },
      ],
    });

    return res.status(200).json(messages);
  }
  catch(e) { return next(e); }
});

router.post('/to/:id', mids.postTo, async (req, res, next) => {
  try {
    let target = await User.findById(req.params.id);
    if (!target) return res.status(404).json({ error: 'User not found' });

    if (!await Match.findOne({ users: { $all: [ req.user._id, target._id ] }}))
      return res.status(400).json({ error: 'You are not a match' });

    let msg = new Message({
      from: req.user._id,
      to: target._id,
      text: req.body.text,
    });

    await msg.save();

    req.$ss.sendMessage(msg);

    return res.status(200).json({});
  }
  catch(e) { return next(e); }
});

router.post('/read_all/:from', guard, async (req, res, next) => {
  try {
    let target = await User.findById(req.params.from);
    if (!target) return res.status(404).json({ error: 'User not found' });

    await Message.updateMany({ from: target._id, to: req.user._id }, {
      $set: { read: true },
    });

    return res.status(200).json({});
  }
  catch(e) { return next(e); }
});

module.exports = router;
