const router  = require('express').Router();

const routes = [
  { url: '/user', path: 'user.js' },
  { url: '/auth', path: 'auth.js' },
  { url: '/admin', path: 'admin.js' },
  { url: '/message', path: 'message.js' },
  // { url: '/post', path: 'post.js' },
];

for (const route of routes) {
  // if (process.env.DEV)
  //   console.log(`Route /api${route.url} connected.`);
  router.use(route.url, require(`./routes/${route.path}`));
}

router.use((err, req, res, next) => {
  next; req;
  err;
  return res.status(500).json({ error: 'INTERNAL_ERROR' });
});

module.exports = router;
